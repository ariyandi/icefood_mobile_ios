//
//  IFDatabaseManager.h
//  IceFood
//
//  Created by Ariyandi Widarto on 5/29/17.
//  Copyright © 2017 Ice House. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IFDatabaseManager : NSObject

@property (strong, nonatomic) NSMutableArray *arrColumnNames;
@property (assign, nonatomic) int affectedRows;
@property (assign, nonatomic) long long lastInsertedRowID;

- (instancetype)initWithDatabaseFilename:(NSString *)dbFilename;

- (NSArray *)loadDataFromDB:(NSString *)query;
- (void)executeQuery:(NSString *)query;

@end
