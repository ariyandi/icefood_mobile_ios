//
//  IFAppDelegate.m
//  IceFood
//
//  Created by Ariyandi Widarto on 5/29/17.
//  Copyright © 2017 Ice House. All rights reserved.
//

#import "IFAppDelegate.h"

#import "IFHomeTabBarController.h"

@interface IFAppDelegate ()

@end

@implementation IFAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
	
	self.window.rootViewController = [self rootViewController];
	[self.window makeKeyAndVisible];
	
	return YES;
}

- (UIWindow *)window {
	
	if (_window == nil) {
		CGRect mainFrame = [UIScreen mainScreen].bounds;
		_window = [[UIWindow alloc] initWithFrame:mainFrame];
	}
	
	return _window;
}

- (UIViewController *)rootViewController {
	
	IFHomeTabBarController *homeTabBarController = [[IFHomeTabBarController alloc] init];
	UINavigationController *mainNavigationController = [[UINavigationController alloc] initWithRootViewController:homeTabBarController];
	
	return mainNavigationController;
}

@end
