//
//  IFOrderByPersonViewController.m
//  IceFood
//
//  Created by Ariyandi Widarto on 5/29/17.
//  Copyright © 2017 Ice House. All rights reserved.
//

#import "IFOrderByPersonViewController.h"

#import "UIViewController+NavigationBarButtonClickable.h"
#import "IFMenuByDateViewController.h"

@interface IFOrderByPersonViewController ()

@end

@implementation IFOrderByPersonViewController

- (void)viewWillAppear:(BOOL)animated {
	
	[super viewWillAppear:animated];
	
	[self.tabBarController.navigationItem setTitle:@"Order by Person"];
	
	[self configureTabBarNavigationBarWithLeftBarButton:Settings
										 rightBarButton:NewOrder];
}

#pragma mark - NavigationBarButtonClickable

- (void)leftButtonClicked:(id)sender {
	
	IFMenuByDateViewController *menuByDateViewController = [[IFMenuByDateViewController alloc] init];
	
	[self.navigationController pushViewController:menuByDateViewController
										 animated:YES];
}

- (void)rightButtonClicked:(id)sender {
	
}

@end
