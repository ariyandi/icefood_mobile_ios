//
//  IFOrderListViewController.m
//  IceFood
//
//  Created by Ariyandi Widarto on 5/29/17.
//  Copyright © 2017 Ice House. All rights reserved.
//

#import "IFOrderListViewController.h"

#import "UIViewController+NavigationBarButtonClickable.h"
#import "IFMenuByDateViewController.h"

@interface IFOrderListViewController ()

@end

@implementation IFOrderListViewController

- (void)viewWillAppear:(BOOL)animated {
	
    [super viewWillAppear:animated];
	
	[self.tabBarController.navigationItem setTitle:@"Current Order"];
	
	[self configureTabBarNavigationBarWithLeftBarButton:Settings
									rightBarButton:NewOrder];
}

#pragma mark - NavigationBarButtonClickable

- (void)leftButtonClicked:(id)sender {
	
	IFMenuByDateViewController *menuByDateViewController = [[IFMenuByDateViewController alloc] init];
	
	[self.navigationController pushViewController:menuByDateViewController
										 animated:YES];
}

- (void)rightButtonClicked:(id)sender {
	
}

@end
