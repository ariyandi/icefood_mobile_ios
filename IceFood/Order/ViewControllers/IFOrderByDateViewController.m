//
//  IFOrderByDateViewController.m
//  IceFood
//
//  Created by Ariyandi Widarto on 5/29/17.
//  Copyright © 2017 Ice House. All rights reserved.
//

#import "IFOrderByDateViewController.h"

#import "UIViewController+NavigationBarButtonClickable.h"
#import "IFMenuByDateViewController.h"

@interface IFOrderByDateViewController ()

@end

@implementation IFOrderByDateViewController

- (void)viewWillAppear:(BOOL)animated {
	
	[super viewWillAppear:animated];
	
	[self.tabBarController.navigationItem setTitle:@"Order by Date"];
	
	[self configureTabBarNavigationBarWithLeftBarButton:Settings
										 rightBarButton:NewOrder];
}

#pragma mark - NavigationBarButtonClickable

- (void)leftButtonClicked:(id)sender {
	
	IFMenuByDateViewController *menuByDateViewController = [[IFMenuByDateViewController alloc] init];
	
	[self.navigationController pushViewController:menuByDateViewController
										 animated:YES];
}

- (void)rightButtonClicked:(id)sender {
	
}

@end
