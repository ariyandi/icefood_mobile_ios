//
//  IFHomeTabBarController.m
//  IceFood
//
//  Created by Ariyandi Widarto on 5/29/17.
//  Copyright © 2017 Ice House. All rights reserved.
//

#import "IFHomeTabBarController.h"

#import "IFOrderListViewController.h"
#import "IFOrderByDateViewController.h"
#import "IFOrderByPersonViewController.h"

@interface IFHomeTabBarController ()

@end

@implementation IFHomeTabBarController

- (void)viewDidLoad {
	
	[super viewDidLoad];
	
	[self configureViewControllers];
}

- (void)configureViewControllers {
	
	IFOrderListViewController *orderListViewController = [[IFOrderListViewController alloc] init];
	orderListViewController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Current Order"
																	   image:[UIImage imageNamed:@"order_list"]
															   selectedImage:[UIImage imageNamed:@"order_list"]];
	
	IFOrderByDateViewController *orderByDateViewController = [[IFOrderByDateViewController alloc] init];
	orderByDateViewController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Order by Date"
																		 image:[UIImage imageNamed:@"order_by_date"]
																 selectedImage:[UIImage imageNamed:@"order_by_date"]];
	
	IFOrderByPersonViewController *orderByPersonViewController = [[IFOrderByPersonViewController alloc] init];
	orderByPersonViewController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Order by Person"
																		   image:[UIImage imageNamed:@"order_by_person"]
																   selectedImage:[UIImage imageNamed:@"order_by_person"]];
	
	NSArray *viewControllers = @[
		orderListViewController,
		orderByDateViewController,
		orderByPersonViewController
	];
	
	[self setViewControllers:viewControllers];
}

@end
