//
//  UIViewController+NavigationBarButtonClickable.h
//  IceFood
//
//  Created by Ariyandi Widarto on 5/29/17.
//  Copyright © 2017 Ice House. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
	Settings,
	Back,
	NewOrder,
	NewMenu,
	Save,
	None
} IFNavigationBarButton;

@interface UIViewController (NavigationBarButtonClickable)

- (void)configureViewControllerNavigationBarWithLeftBarButton:(IFNavigationBarButton)leftBarButton
											   rightBarButton:(IFNavigationBarButton)rightBarButton;

- (void)configureTabBarNavigationBarWithLeftBarButton:(IFNavigationBarButton)leftBarButton
									   rightBarButton:(IFNavigationBarButton)rightBarButton;

@end
