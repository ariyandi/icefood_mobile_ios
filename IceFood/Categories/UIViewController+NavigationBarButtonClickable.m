//
//  UIViewController+NavigationBarButtonClickable.m
//  IceFood
//
//  Created by Ariyandi Widarto on 5/29/17.
//  Copyright © 2017 Ice House. All rights reserved.
//

#import "UIViewController+NavigationBarButtonClickable.h"

typedef enum {
	Left,
	Right
} IFBarButtonItemPosition;

@implementation UIViewController (NavigationBarButtonClickable)

- (void)configureViewControllerNavigationBarWithLeftBarButton:(IFNavigationBarButton)leftBarButton
											   rightBarButton:(IFNavigationBarButton)rightBarButton {
	
	UIViewController *viewController = self;
	
	[self configureNavigationBarInViewController:viewController
								   leftBarButton:leftBarButton
								  rightBarButton:rightBarButton];
}

- (void)configureTabBarNavigationBarWithLeftBarButton:(IFNavigationBarButton)leftBarButton
									   rightBarButton:(IFNavigationBarButton)rightBarButton {
	
	UIViewController *viewController = self.tabBarController;
	
	[self configureNavigationBarInViewController:viewController
								   leftBarButton:leftBarButton
								  rightBarButton:rightBarButton];
}

- (void)configureNavigationBarInViewController:(UIViewController *)viewController
								 leftBarButton:(IFNavigationBarButton)leftBarButton
								rightBarButton:(IFNavigationBarButton)rightBarButton {
	
	switch (leftBarButton) {
		case Settings:
			viewController.navigationItem.leftBarButtonItem = [self getSettingsButtonWithPosition:Left];
			break;
		case Back:
			viewController.navigationItem.leftBarButtonItem = [self getBackButtonWithPosition:Left];
			break;
		case NewOrder:
			viewController.navigationItem.leftBarButtonItem = [self getNewOrderButtonWithPosition:Left];
			break;
		case NewMenu:
			viewController.navigationItem.leftBarButtonItem = [self getNewMenuButtonWithPosition:Left];
			break;
		case Save:
			viewController.navigationItem.leftBarButtonItem = [self getSaveButtonWithPosition:Left];
			break;
		case None:
			break;
	}
	
	switch (rightBarButton) {
		case Settings:
			viewController.navigationItem.rightBarButtonItem = [self getSettingsButtonWithPosition:Right];
			break;
		case Back:
			viewController.navigationItem.rightBarButtonItem = [self getBackButtonWithPosition:Right];
			break;
		case NewOrder:
			viewController.navigationItem.rightBarButtonItem = [self getNewOrderButtonWithPosition:Right];
			break;
		case NewMenu:
			viewController.navigationItem.rightBarButtonItem = [self getNewMenuButtonWithPosition:Right];
			break;
		case Save:
			viewController.navigationItem.rightBarButtonItem = [self getSaveButtonWithPosition:Right];
			break;
		case None:
			break;
	}
}

- (void)leftButtonClicked:(id)sender {
	
}

- (void)rightButtonClicked:(id)sender {
	
}

- (SEL)getButtonActionForPosition:(IFBarButtonItemPosition)position {
	SEL buttonAction;
	
	switch (position) {
		case Left:
			buttonAction = @selector(leftButtonClicked:);
			break;
		case Right:
			buttonAction = @selector(rightButtonClicked:);
			break;
	}
	
	return buttonAction;
}

- (UIBarButtonItem *)getSettingsButtonWithPosition:(IFBarButtonItemPosition)position {
	SEL buttonAction = [self getButtonActionForPosition:position];
	
	return [[UIBarButtonItem alloc] initWithTitle:@"Settings"
											style:UIBarButtonItemStylePlain
										   target:self
										   action:buttonAction];
}

- (UIBarButtonItem *)getBackButtonWithPosition:(IFBarButtonItemPosition)position {
	SEL buttonAction = [self getButtonActionForPosition:position];
	
	return [[UIBarButtonItem alloc] initWithTitle:@"Back"
											style:UIBarButtonItemStylePlain
										   target:self
										   action:buttonAction];
}

- (UIBarButtonItem *)getNewOrderButtonWithPosition:(IFBarButtonItemPosition)position {
	SEL buttonAction = [self getButtonActionForPosition:position];
	
	return [[UIBarButtonItem alloc] initWithTitle:@"New Order"
											style:UIBarButtonItemStylePlain
										   target:self
										   action:buttonAction];
}

- (UIBarButtonItem *)getNewMenuButtonWithPosition:(IFBarButtonItemPosition)position {
	SEL buttonAction = [self getButtonActionForPosition:position];
	
	return [[UIBarButtonItem alloc] initWithTitle:@"New Menu"
											style:UIBarButtonItemStylePlain
										   target:self
										   action:buttonAction];
}

- (UIBarButtonItem *)getSaveButtonWithPosition:(IFBarButtonItemPosition)position {
	SEL buttonAction = [self getButtonActionForPosition:position];
	
	return [[UIBarButtonItem alloc] initWithTitle:@"Save"
											style:UIBarButtonItemStylePlain
										   target:self
										   action:buttonAction];
}

@end
