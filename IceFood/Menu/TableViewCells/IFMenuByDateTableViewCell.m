//
//  IFMenuByDateTableViewCell.m
//  IceFood
//
//  Created by Ariyandi Widarto on 5/30/17.
//  Copyright © 2017 Ice House. All rights reserved.
//

#import "IFMenuByDateTableViewCell.h"

@interface IFMenuByDateTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation IFMenuByDateTableViewCell

- (void)prepareForReuse {
	[super prepareForReuse];
	
	[self resetAppearance];
}

+ (NSString *)identifierName {
	return @"IFMenuByDateTableViewCell";
}

+ (CGFloat)preferredHeight {
	return 44.0;
}

- (void)resetAppearance {
	self.titleLabel.text = @"";
}

- (void)setTitle:(NSString *)title {
	self.titleLabel.text = title;
}

@end
