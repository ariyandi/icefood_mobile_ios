//
//  IFMenuByDateTableViewCell.h
//  IceFood
//
//  Created by Ariyandi Widarto on 5/30/17.
//  Copyright © 2017 Ice House. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IFMenuByDateTableViewCell : UITableViewCell

+ (NSString *)identifierName;
+ (CGFloat)preferredHeight;

- (void)setTitle:(NSString *)title;

@end
