//
//  IFMenuListTableViewCell.m
//  IceFood
//
//  Created by Ariyandi Widarto on 5/30/17.
//  Copyright © 2017 Ice House. All rights reserved.
//

#import "IFMenuListTableViewCell.h"

@interface IFMenuListTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *subtitle;

@end

@implementation IFMenuListTableViewCell

- (void)prepareForReuse {
	[super prepareForReuse];
	
	[self resetAppearance];
}

+ (NSString *)identifierName {
	return @"IFMenuListTableViewCell";
}

+ (CGFloat)preferredHeight {
	return 60.0;
}

- (void)resetAppearance {
	self.title.text = @"";
	self.subtitle.text = @"";
}

- (void)setTitle:(NSString *)title
		subtitle:(NSString *)subtitle {
	
	self.title.text = title;
	self.subtitle.text = subtitle;
}

@end
