//
//  IFMenuListTableViewCell.h
//  IceFood
//
//  Created by Ariyandi Widarto on 5/30/17.
//  Copyright © 2017 Ice House. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IFMenuListTableViewCell : UITableViewCell

+ (NSString *)identifierName;
+ (CGFloat)preferredHeight;

- (void)setTitle:(NSString *)title
		subtitle:(NSString *)subtitle;

@end
