//
//  IFMenuByDateViewController.m
//  IceFood
//
//  Created by Ariyandi Widarto on 5/30/17.
//  Copyright © 2017 Ice House. All rights reserved.
//

#import "IFMenuByDateViewController.h"

#import "UIViewController+NavigationBarButtonClickable.h"
#import "IFMenuByDateTableViewCell.h"
#import "IFMenuListViewController.h"

@interface IFMenuByDateViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation IFMenuByDateViewController

- (void)viewDidLoad {
	
	[super viewDidLoad];
	
	self.title = @"Menu by Date";
	
	[self configureViewControllerNavigationBarWithLeftBarButton:Back
												 rightBarButton:None];
	
	[self configureTableView];
}

- (void)configureTableView {
	UINib *menuByDateTableViewCellNib = [UINib nibWithNibName:[IFMenuByDateTableViewCell identifierName]
													   bundle:nil];
	
	[self.tableView registerNib:menuByDateTableViewCellNib
		 forCellReuseIdentifier:[IFMenuByDateTableViewCell identifierName]];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return [IFMenuByDateTableViewCell preferredHeight];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(IFMenuByDateTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
	
	switch (indexPath.row) {
		case Monday:
			[cell setTitle:@"Monday"];
			break;
		case Tuesday:
			[cell setTitle:@"Tuesday"];
			break;
		case Wednesday:
			[cell setTitle:@"Wednesday"];
			break;
		case Thursday:
			[cell setTitle:@"Thursday"];
			break;
		case Friday:
			[cell setTitle:@"Friday"];
			break;
		default:
			break;
	}
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	IFDay day = indexPath.row;
	
	IFMenuListViewController *menuListViewController = [[IFMenuListViewController alloc] initWithDay:day];
	
	[self.navigationController pushViewController:menuListViewController
										 animated:YES];
}

#pragma mark - UITableViewDataScource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	return [tableView dequeueReusableCellWithIdentifier:[IFMenuByDateTableViewCell identifierName]
										   forIndexPath:indexPath];
}

#pragma mark - NavigationBarButtonClickable

- (void)leftButtonClicked:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)rightButtonClicked:(id)sender {
	
}

@end
