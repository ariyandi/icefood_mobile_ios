//
//  IFMenuViewController.m
//  IceFood
//
//  Created by Ariyandi Widarto on 5/30/17.
//  Copyright © 2017 Ice House. All rights reserved.
//

#import "IFMenuViewController.h"

#import "UIViewController+NavigationBarButtonClickable.h"
#import "IFDatabaseManager.h"

@interface IFMenuViewController ()

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *priceTextField;

@property (assign, nonatomic) int menuID;
@property (assign, nonatomic) IFDay day;
@property (strong, nonatomic) IFDatabaseManager *databaseManager;

@end

@implementation IFMenuViewController

- (instancetype)initWithMenuID:(int)menuID
						   day:(IFDay)day {
	
	self = [self initWithNibName:@"IFMenuViewController"
						  bundle:nil];
	
	if (self != nil) {
		
		_databaseManager = [[IFDatabaseManager alloc] initWithDatabaseFilename:@"IceFood.sql"];
		_menuID = menuID;
		_day = day;
	}
	
	return self;
}

- (void)viewDidLoad {
	
	[super viewDidLoad];
	
	self.title = @"Menu";
	
	[self configureViewControllerNavigationBarWithLeftBarButton:Back
												 rightBarButton:Save];
	
	if (self.menuID != -1) {
		
		[self loadMenuToEdit];
	}
}

- (void)loadMenuToEdit {
	
	NSString *query = [NSString stringWithFormat:@"SELECT * FROM Menu where menuID = %@", @(self.menuID)];
	
	NSArray *results = [[NSArray alloc] initWithArray:[self.databaseManager loadDataFromDB:query]];
	
	self.nameTextField.text = [[results objectAtIndex:0] objectAtIndex:[self.databaseManager.arrColumnNames indexOfObject:@"name"]];
	self.priceTextField.text = [[results objectAtIndex:0] objectAtIndex:[self.databaseManager.arrColumnNames indexOfObject:@"price"]];
}

- (void)saveMenu {
	
	NSString *query;
	if (self.menuID == -1) {
		query = [NSString stringWithFormat:@"INSERT INTO Menu VALUES(null, '%@', '%@', %@)", self.nameTextField.text, self.priceTextField.text, @(self.day)];
	} else {
		query = [NSString stringWithFormat:@"UPDATE Menu SET name='%@', price='%@' WHERE menuID = %@", self.nameTextField.text, self.priceTextField.text, @(self.menuID)];
	}
	
	[self.databaseManager executeQuery:query];
	
	if (self.databaseManager.affectedRows != 0) {
		
		[self.delegate onCompletedEditingMenu];
		
		[self.navigationController popViewControllerAnimated:YES];
	} else {
		NSLog(@"Could not execute the query.");
	}
}

#pragma mark - NavigationBarButtonClickable

- (void)leftButtonClicked:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)rightButtonClicked:(id)sender {
	[self saveMenu];
}

@end
