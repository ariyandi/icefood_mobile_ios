//
//  IFMenuListViewController.h
//  IceFood
//
//  Created by Ariyandi Widarto on 5/30/17.
//  Copyright © 2017 Ice House. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSInteger {
	Monday = 0,
	Tuesday,
	Wednesday,
	Thursday,
	Friday,
	Saturday,
	Sunday
} IFDay;

@interface IFMenuListViewController : UIViewController

- (instancetype)initWithDay:(IFDay)day;

@end
