//
//  IFMenuListViewController.m
//  IceFood
//
//  Created by Ariyandi Widarto on 5/30/17.
//  Copyright © 2017 Ice House. All rights reserved.
//

#import "IFMenuListViewController.h"

#import "UIViewController+NavigationBarButtonClickable.h"
#import "IFDatabaseManager.h"
#import "IFMenuListTableViewCell.h"
#import "IFMenuViewController.h"

@interface IFMenuListViewController () <UITableViewDelegate, UITableViewDataSource, IFMenuViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) IFDatabaseManager *databaseManager;
@property (strong, nonatomic) NSArray *menu;

@property (assign, nonatomic) IFDay day;

@end

@implementation IFMenuListViewController

- (instancetype)initWithDay:(IFDay)day {
	
	self = [self initWithNibName:@"IFMenuListViewController"
						  bundle:nil];
	
	if (self != nil) {
		
		_databaseManager = [[IFDatabaseManager alloc] initWithDatabaseFilename:@"IceFood.sql"];
		_day = day;
	}
	
	return self;
}

- (void)viewDidLoad {
	
	[super viewDidLoad];
	
	self.title = @"Menu List";
	
	[self configureViewControllerNavigationBarWithLeftBarButton:Back
												 rightBarButton:NewMenu];
	
	[self configureTableView];
	[self loadData];
}

- (void)configureTableView {
	UINib *menuListTableViewCellNib = [UINib nibWithNibName:[IFMenuListTableViewCell identifierName]
													 bundle:nil];
	
	[self.tableView registerNib:menuListTableViewCellNib
		 forCellReuseIdentifier:[IFMenuListTableViewCell identifierName]];
}

- (void)loadData {
	
	NSString *query = [NSString stringWithFormat:@"SELECT * FROM Menu WHERE Day = %@", @(self.day)] ;
	
	if (self.menu != nil) {
		self.menu = nil;
	}
	self.menu = [[NSArray alloc] initWithArray:[self.databaseManager loadDataFromDB:query]];
	
	[self.tableView reloadData];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return [IFMenuListTableViewCell preferredHeight];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(IFMenuListTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
	
	// TODO: Find a better way to parse the SQLite query result into a model.
	NSInteger indexOfName = [self.databaseManager.arrColumnNames indexOfObject:@"name"];
	NSInteger indexOfPrice = [self.databaseManager.arrColumnNames indexOfObject:@"price"];
	
	NSString *title = [NSString stringWithFormat:@"%@", [[self.menu objectAtIndex:indexPath.row] objectAtIndex:indexOfName]];
	NSString *subtitle = [NSString stringWithFormat:@"Price: %@", [[self.menu objectAtIndex:indexPath.row] objectAtIndex:indexOfPrice]];
	
	[cell setTitle:title
		  subtitle:subtitle];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	int menuID = [[[self.menu objectAtIndex:indexPath.row] objectAtIndex:0] intValue];
	
	IFMenuViewController *menuViewController = [[IFMenuViewController alloc] initWithMenuID:menuID
																						day:self.day];
	menuViewController.delegate = self;
	
	[self.navigationController pushViewController:menuViewController
										 animated:YES];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
	
	if (editingStyle == UITableViewCellEditingStyleDelete) {
		
		int menuIDToDelete = [[[self.menu objectAtIndex:indexPath.row] objectAtIndex:0] intValue];
		
		NSString *query = [NSString stringWithFormat:@"DELETE FROM Menu WHERE menuID = %@", @(menuIDToDelete)];
		
		[self.databaseManager executeQuery:query];
		
		[self loadData];
	}
}

#pragma mark - UITableViewDataScource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return self.menu.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	return [tableView dequeueReusableCellWithIdentifier:[IFMenuListTableViewCell identifierName]
										   forIndexPath:indexPath];
}

#pragma mark - IFMenuViewControllerDelegate

- (void)onCompletedEditingMenu {
	[self loadData];
}

#pragma mark - NavigationBarButtonClickable

- (void)leftButtonClicked:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)rightButtonClicked:(id)sender {
	IFMenuViewController *menuViewController = [[IFMenuViewController alloc] initWithMenuID:-1
																						day:self.day];
	menuViewController.delegate = self;
	
	[self.navigationController pushViewController:menuViewController
										 animated:YES];
}

@end
