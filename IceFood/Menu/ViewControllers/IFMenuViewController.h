//
//  IFMenuViewController.h
//  IceFood
//
//  Created by Ariyandi Widarto on 5/30/17.
//  Copyright © 2017 Ice House. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IFMenuListViewController.h"

@protocol IFMenuViewControllerDelegate

- (void)onCompletedEditingMenu;

@end

@interface IFMenuViewController : UIViewController

@property (strong, nonatomic) id<IFMenuViewControllerDelegate> delegate;

- (instancetype)initWithMenuID:(int)menuID
						   day:(IFDay)day;

@end
